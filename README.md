# WinkyLink Provider for Laravel Socialite
#### Author: WinkyLink.com

### 1. Create your application in [https://winkylink.com](https://winkylink.com)
### 2. Get your Client Id and Client Secret code


### 3. Register plugin in your AppServiceProvider

```
public function boot(): void
    {
        try {
            $socialite = $this->app->make(Factory::class);
            $socialite->extend('winkylink', static function () use ($socialite) {
                return $socialite->buildProvider(SocialiteWinkyLinkProvider::class, config('services.winkylink'));
            });
        } catch (BindingResolutionException $e) {
        }
    }
```

### 4. Add config in your config/service.php 
```
'winkylink' => [
    'client_id' => '',
    'client_secret' => '',
    'redirect' => env('APP_URL', '').'/callback/winkylink',
],
```

### 5. Create your routes 

```
Route::get('auth/redirect/{provider}', 'SocialController@redirect');
Route::get('callback/{provider}', 'SocialController@callback');
```

### 6. Create your controller like this
```
class SocialController extends BaseController
{
    /**
     * @param string $provider
     * @return RedirectResponse
     */
    public function redirect(string $provider): RedirectResponse
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param string $provider
     * @return RedirectResponse
     */
    public function callback(string $provider): RedirectResponse
    {
        try {
            $getInfo = Socialite::driver($provider)->user();
            $user = $this->createUser($getInfo, $provider);
            auth()->login($user);
            return redirect()->route('index');
        } catch (Throwable $e) {
            return redirect('auth/redirect/' . $provider);
        }
    }
    
    /**
     * @param mixed $getInfo
     * @param string $provider
     * @return User
     */
    private function createUser($getInfo, string $provider): User
    {
        // Create your user model using $getInfo array
         return $user;
    }
    
}
```

### 7. Add to your blade login button like this
```angular2html
<a href="https://yourwebsite.com/auth/redirect/winkylink">Login with WinkyLink</a>
```