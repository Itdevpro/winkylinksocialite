<?php

namespace Winkylink\Socialite;

use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\User;

/**
 * WinkyLink Socialite provider
 */
class SocialiteWinkyLinkProvider extends AbstractProvider

{
    /**
     * @var string[]
     */
    protected $scopes = [
        'id',
        'email',
        'name',
        'photo',
    ];

    /**
     * @return string
     */
    public function getWinkyLinkUrl(): string
    {
        return 'https://winkylink.com/api/apps';
    }

    /**
     * @param $state
     * @return string
     */
    protected function getAuthUrl($state): string
    {
        return $this->buildAuthUrlFromBase($this->getWinkyLinkUrl() . '/auth', $state);
    }

    /**
     * @return string
     */
    protected function getTokenUrl(): string
    {
        return $this->getWinkyLinkUrl() . '/token';
    }

    /**
     * @param $token
     * @return array
     * @throws GuzzleException
     * @throws JsonException
     */
    protected function getUserByToken($token): array
    {
        $response = $this->getHttpClient()->post($this->getWinkyLinkUrl() . '/userInfo', [
            'headers' => [
                'cache-control' => 'no-cache',
                'Authorization' => $token,
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param array $user
     * @return User
     */
    protected function mapUserToObject(array $user): User
    {
        return (new User())->setRaw($user)->map([
            'id' => $user['id'] ?? null,
            'nickname' => $user['username'] ?? null,
            'name' => $user['name'] ?? null,
            'email' => $user['email'] ?? null,
            'avatar' => $user['avatar'] ?? null,
        ]);
    }
}